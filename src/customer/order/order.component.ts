import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderService } from 'src/customer/order/order.service';
import { Store } from '@ngrx/store';
import * as appActions from '../../app/app.actions';
import * as appReducers from '../../app/app.reducers';
import { AppState } from 'src/app/Model/app.state.model';
import { Observable } from 'rxjs';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Order } from '../../app/Model/customer.model';
import { AnyFn } from '@ngrx/store/src/selector';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  noOrders: boolean;
  displayedColumns: string[] = ['orderId', 'orderdDate', 'delieverdDate', 'totalPrice', 'actions'];
  pageSizeOptions = [10, 20];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  customerOrders: MatTableDataSource<Order>;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private orderService: OrderService,
              private store: Store<AppState>) {
    this.noOrders = false;
  }
  ngOnInit() {
    // To get the orders fetched by resolve
    this.activatedRoute.data.subscribe(data => {
      if (data.cres.ordersDatabase[0].orders === undefined) {
        this.noOrders = true;
      }
      this.customerOrders = new MatTableDataSource(data.cres.ordersDatabase[0].orders || []);
    });
  }
  getOrderDetails(orderDetails) {
    sessionStorage.setItem('orderDetail', JSON.stringify(orderDetails));
    this.router.navigate(['../orderDetail'], { relativeTo: this.activatedRoute });
  }
  applyFilter(filterValue: string) {
    this.customerOrders.filter = filterValue;
  }
}
