import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as appActions from '../../app/app.actions';
import * as constants from '../../app/constants';
import { AppState } from 'src/app/Model/app.state.model';
import { Order } from '../../app/Model/customer.model';
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private http: HttpClient, private store: Store<AppState>) {
  }
  loadOrders$(customerId): Observable<Order[]> {
    return this.http.get(constants.baseURL + 'ordersDatabase').pipe(map((response: any) => {
      const data = response.ordersDatabase.filter((item) => {
        if (item.customerId === customerId) {
          return item;
        }
      });
      return data[0].orders;
    }));
  }
}
