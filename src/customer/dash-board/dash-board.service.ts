import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as constants from '../../app/constants';

@Injectable({
  providedIn: 'root'
})
export class DashBoardService {

  constructor(private http: HttpClient) {
  }
  loadProducts$(): Observable<any> {
    return this.http.get(constants.baseURL + 'loadProducts');
  }
}
