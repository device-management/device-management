import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as model from 'src/app/Model/customer.model';
import { DashBoardService } from './dash-board.service';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {
  private products: model.Product[];
  constructor(private router: Router, private dashBoardService: DashBoardService) { }

  ngOnInit() {
    // Fetch the products from the database
    this.dashBoardService.loadProducts$().subscribe(data => {
      this.products = data.products;
    });
  }

}
