import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AppState } from 'src/app/Model/app.state.model';
import { Customer } from 'src/app/Model/customer.model';
import { CustomerService } from 'src/customer/customer.service';

import * as appActions from '../../app/app.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  cartProductsLength: number;
  customer$: Observable<Customer>;
  customer: Customer;
  constructor(private store: Store<AppState>, private customerService: CustomerService, private router: Router) {
    // Retrieve the customer details
    this.customer$ = this.store.select((state) => state.store.customer);
  }

  ngOnInit() {
    this.customer$.subscribe(customer => {
      this.customer = customer;
      // Fetch the cart info
      this.customerService.getCart(this.customer.customerId);
    });
    this.customerService.CartState.subscribe((data) => {
      this.cartProductsLength = data.length;
    });
  }
  logout() {
    this.store.dispatch(new appActions.LogOut({}));
    this.router.navigateByUrl('login');
  }
}
