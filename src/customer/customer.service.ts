import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppState } from 'src/app/Model/app.state.model';
import { Store } from '@ngrx/store';
import * as appActions from '../app/app.actions';
import * as constants from '../app/constants';
import * as model from 'src/app/Model/customer.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService implements OnInit {
  order;
  customerId;
  products: model.Product[];
  // cart subject to synchronous
  public cartSubject = new BehaviorSubject<model.Product[]>([]);
  CartState = this.cartSubject.asObservable();
  constructor(private http: HttpClient, private store: Store<AppState>) {
  }
  ngOnInit() {
  }
  addToCart(cartProduct: model.Product) {
    this.products = this.cartSubject.value;
    const index = this.products.findIndex(product => product.productId === cartProduct.productId);
    if (index === -1) {
      const updatedValue = [...this.products, cartProduct];
      this.cartSubject.next(updatedValue);
      this.store.dispatch(new appActions.UpdateCart(updatedValue));
    } else {
      alert('product already added to cart');
    }
  }
  saveCart(cartProducts) {
    this.cartSubject.next(cartProducts);
    this.store.dispatch(new appActions.UpdateCart(cartProducts));
  }

  removeFromCart(cartProduct) {
    this.products = this.cartSubject.value;
    const index = this.products.findIndex(product => product.productId === cartProduct.productId);
    if (index !== -1) {
      this.products.splice(index, 1);
      const updatedValue = [...this.products];
      this.cartSubject.next(updatedValue);
      this.store.dispatch(new appActions.UpdateCart(this.products));
    }
  }
  placeOrder(products, price) {
    // creating a new order object
    this.order = constants.order;
    this.order.products = products;
    this.order.totalPrice = price;
    // A call to store the order in database should be made

    // clearing the cart
    this.products = [];
    this.cartSubject.next(this.products);
    this.store.dispatch(new appActions.UpdateCart(this.products));
  }
  getCart(customerId) {
    this.http.get(constants.baseURL + 'ordersDatabase').subscribe((response: any) => {
      const data = response.ordersDatabase.filter((item) => {
        if (item.customerId === customerId) {
          return item;
        }
      });
      // if (data[0].cartInfo === undefined) {
      //   data[0].cartInfo = [];
      // }
      this.store.dispatch(new appActions.UpdateCart(data[0].cartInfo || []));
      this.cartSubject.next(data[0].cartInfo || []);
    });
  }
}

