import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import * as appActions from '../../app/app.actions';
import * as model from '../../app/Model/customer.model';
import { AppState } from 'src/app/Model/app.state.model';
import { CustomerService } from 'src/customer/customer.service';
import { NgModel, FormsModule } from '@angular/forms';
import { MatTable, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild('confirmationDialog') confirmationDialog: any;
  total: number;
  cartProducts: model.Product[];
  displayedColumns: string[] = ['product', 'price', 'quantity', 'action'];
  constructor(private store: Store<AppState>, private customerService: CustomerService, private router: Router,
              private dialog: MatDialog, private activatedRoute: ActivatedRoute) {
    this.total = 0;
  }
  ngOnInit() {
    this.cartProducts = [];
    this.customerService.CartState.subscribe((data) => {
      this.cartProducts = data;
      this.addTotal();
    });
  }
  removeFromCart(product) {
    this.customerService.removeFromCart(product);
  }
  addTotal() {
    this.total = 0;
    this.cartProducts.forEach(element => {
      this.total = this.total + (element.price * element.quantity);
    });
  }
  saveCart() {
    this.customerService.saveCart(this.cartProducts);
    alert('successfully saved');
    this.router.navigateByUrl('home/dashboard');
   //  this.customerService.confirm('' , 'Products Saved Successfully', '../home');
  }
  placeOrder() {
    const dialogRef = this.dialog.open(this.confirmationDialog);
    dialogRef.afterClosed().subscribe(result => {
     if (result) {
      this.customerService.placeOrder(this.cartProducts, this.total);
      this.router.navigate(['../dashboard'], { relativeTo: this.activatedRoute });
    }
    });
  }
}
