import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Order } from '../../app/Model/customer.model';
@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
  currentOrder: Order;
  constructor() { }

  ngOnInit() {
    this.currentOrder = JSON.parse(sessionStorage.getItem('orderDetail'));
  }
}
