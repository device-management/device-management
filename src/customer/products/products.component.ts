import { Component, OnInit, Input } from '@angular/core';
import { CustomerService } from '../customer.service';
import * as model from '../../app/Model/customer.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  @Input() product: model.Customer;
  constructor(private customerService: CustomerService) { }

  ngOnInit() {
  }
  addToCart(product) {
    product.quantity = 1;
    this.customerService.addToCart(product);
  }
}
