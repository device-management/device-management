
import { ActivatedRouteSnapshot } from '@angular/router';
import { RouterStateSnapshot } from '@angular/router';
import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/Model/app.state.model';
import { HttpClient } from '@angular/common/http';
import * as constants from '../app/constants';
import { map } from 'rxjs/operators';
@Injectable()
export class OrderResolver implements Resolve<any> {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): any {
    const customerId = sessionStorage.getItem('cusid');
    return this.http.get(constants.baseURL + 'ordersDatabase');
  }
}
