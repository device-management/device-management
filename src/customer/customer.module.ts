import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CustomerRoutingModule } from './customer.routing';
import { CustomMaterialModule } from '../custom-material/custom-material.module';

import { HomeComponent } from './home/home.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { OrderComponent } from './order/order.component';
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
// import { ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';

import { HeaderComponent } from './header/header.component';
import { SidenavComponent } from './sidenav/sidenav.component';

import { LoginModule } from 'src/login/login.module';
import { CustomerService } from 'src/customer/customer.service';
import { OrderResolver } from './resolve';
@NgModule({
  declarations: [HomeComponent, DashBoardComponent, OrderComponent, ProductsComponent, CartComponent,
    HeaderComponent, SidenavComponent, OrderDetailComponent],
  imports: [
    FlexLayoutModule,
    CommonModule,
    CustomerRoutingModule,
    CustomMaterialModule,
    LoginModule,
    FormsModule
  ],
  providers: [CustomerService, OrderResolver]
})
export class CustomerModule { }
