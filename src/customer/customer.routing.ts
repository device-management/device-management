import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DashBoardComponent } from 'src/customer/dash-board/dash-board.component';
import { SignupComponent } from 'src/login/signup/signup.component';
import { OrderComponent } from 'src/customer/order/order.component';
import { CartComponent } from 'src/customer/cart/cart.component';
import { OrderDetailComponent } from '../customer/order-detail/order-detail.component';
import { OrderResolver } from './resolve';

const routes: Routes = [
  {
    path: '', component: HomeComponent, children: [
      { path: 'dashboard', component: DashBoardComponent },
      { path: 'profile', component: SignupComponent },
      {
        path: 'order',
        component: OrderComponent,
        resolve: { cres: OrderResolver }
      },
      { path: 'cart', component: CartComponent },
      { path: 'orderDetail', component: OrderDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
