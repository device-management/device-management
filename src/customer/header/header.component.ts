import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import * as appActions from '../../app/app.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/Model/app.state.model';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  @Output() public logout = new EventEmitter();
  @Input() cartProductsLength: number;
  constructor(private router: Router, private store: Store<AppState>) { }
  ngOnInit() {
  }
  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }
  cartPage() {
    this.router.navigate(['home/cart']);
  }
}
