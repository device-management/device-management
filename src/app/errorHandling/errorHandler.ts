import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorService } from './error.service';
import { NotificationService } from 'src/app/errorHandling/notification.service';
@Injectable()
export class ErrorsHandler implements ErrorHandler {
    constructor(private errorService: ErrorService, private notifier: NotificationService) {

    }
    handleError(error: Error | HttpErrorResponse) {
        let message;
        if (error instanceof HttpErrorResponse) {
            message = this.errorService.getServerMessage(error);
            this.notifier.showError(message);
        } else {
            message = this.errorService.getClientMessage(error);
            this.notifier.showError(message);
        }
        console.log(error);
    }
}

