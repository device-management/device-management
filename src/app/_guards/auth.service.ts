import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as appActions from '../../app/app.actions';
import { AppState } from 'src/app/Model/app.state.model';

@Injectable({ providedIn: 'root' })
export class AuthService implements CanActivate {
    constructor(private router: Router, private store: Store<AppState>) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.store.select(state1 => state1.store.loggedIn).subscribe((loggedIn) => {
            if (loggedIn) {
                return true;
            } else {
                this.router.navigate(['login']);
                // this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
                return false;
            }
        });
        return true;
    }
}
