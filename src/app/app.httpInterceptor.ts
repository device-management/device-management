import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { throwError, Observable  } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
  constructor() { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: localStorage.getItem('tokenId')
      }
    });
    return next.handle(request).pipe(retry(2));
  }
}
