import { Component } from '@angular/core';

// import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
// import {Keepalive} from '@ng-idle/keepalive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'device-management';
  // constructor(private idle: Idle, private keepalive: Keepalive) {
  //   // sets an idle timeout of 5 seconds, for testing purposes.
  //   idle.setIdle(2);
  //   // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
  //   idle.setTimeout(2);
  //   // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
  //   idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
  //   idle.onTimeoutWarning.subscribe((countdown: number) => {
  //     alert('Timeout Warning - ' + countdown);
  //   });
  //   idle.onTimeout.subscribe(() => {
  //     alert('Timed out!');
  //     this.logout();
  //   });
  //   idle.watch();
  // }
  // logout() {
  //   this.idle.stop();
  //   this.idle.ngOnDestroy(); // includes this.idle.stop() and this.clearInterrupts() both.
  // }
}
