
import { Customer } from './Model/customer.model';
import * as appActions from './app.actions';
import { Action } from '@ngrx/store';
import { AppInitialState } from '../app/constants';

export function LoadCustomerSuccessReducer(state  = AppInitialState , action: appActions.Action) {
  switch (action.type) {
        case appActions.LOAD_CUSTOMER_SUCCESS: {
            return  {...state, customer: action.payload};
        }
        case appActions.LOGGED_IN_SUCCESS: {
            return  {...state, loggedIn : action.payload};
        }
        case appActions.UPDATE_CUSTOMER_DETAILS : {
               return {...state};
        }
        case appActions.UPDATE_CART: {
            return {...state, cartInfo: action.payload};
        }
        case appActions.LOGGED_IN_STATUS: {
            return {...state};
        }
        case appActions.LOG_OUT: {
            return state;
        }
        default: {
            return state;
        }
    }
}
