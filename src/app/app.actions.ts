import * as model from './Model/customer.model';
export const LOAD_CUSTOMER_SUCCESS = 'LOAD_CUSTOMER_SUCCESS';
export const LOGGED_IN_SUCCESS = 'LOGGED_IN_SUCCESS';
export const UPDATE_CUSTOMER_DETAILS = 'UPDATE_CUSTOMER_DETAILS';
export const UPDATE_CART = 'UPDATE_CART';
export const LOGGED_IN_STATUS = 'LOGGED_IN_STATUS';
export const LOG_OUT = 'LOG_OUT';

export class LoadCustomerSuccessAction {
    readonly type = LOAD_CUSTOMER_SUCCESS;
    constructor(public payload: model.Customer) {
    }
}

export class LoggedInSuccessAction {
    readonly type = LOGGED_IN_SUCCESS;
    constructor(public payload: boolean) {
    }

}

export class UpdateCustomerDetailsAction {
    readonly type = UPDATE_CUSTOMER_DETAILS;
    constructor(public payload: model.Customer) {
    }
}

export class UpdateCart {
    readonly type = UPDATE_CART;
    constructor(public payload: model.Product[]) {
    }
}


export class LoggedInStatus {
    readonly type = LOGGED_IN_STATUS;
    constructor(public payload: boolean) {
    }
}
export class LogOut {
    readonly type = LOG_OUT;
    constructor(public payload) {
    }
}

export type Action = LoadCustomerSuccessAction|LoggedInSuccessAction| UpdateCustomerDetailsAction|UpdateCart|LoggedInStatus|LogOut;
