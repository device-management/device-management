// Angular predefined Modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler } from '@angular/core';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';

// Developer defined Modules
import { AppRoutingModule } from './app-routing.module';
import { CustomMaterialModule } from '../custom-material/custom-material.module';

// Components & Store
import { AppComponent } from './app.component';
import { LoadCustomerSuccessReducer } from 'src/app/app.reducers';
import { AppHttpInterceptor } from 'src/app/app.httpInterceptor';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ErrorsHandler } from 'src/app/errorHandling/errorHandler';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    CustomMaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot({ store: LoadCustomerSuccessReducer }),
    // NgIdleKeepaliveModule.forRoot()
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AppHttpInterceptor,
    multi: true
  },
  {
    provide: ErrorHandler,
    useClass: ErrorsHandler,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
