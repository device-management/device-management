import * as model from './customer.model';
export interface AppState {
    store: AppStateData;
}

export interface AppStateData {
    customer: model.Customer;
    loggedIn: boolean;
    cartInfo: model.Product[];
    orders: model.Order[];
}
