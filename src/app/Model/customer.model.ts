export interface Customer {
    firstName?: string;
    lastName?: string;
    gender?: string;
    email?: string;
    phone: string;
    password?: string;
    customerId?: string;
}

export interface Product {
    productId: string;
    productName: string;
    price: number;
    productImage: string;
    quantity?: number;
}

export interface Order {
    orderId?: string;
    orderedDate: Date;
    products: Product[];
    totalPrice: number;
    billingAddress: string;
    deliveredDate?: Date;
    status: string;
}
