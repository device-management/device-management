import * as model from './Model/customer.model';
export const baseURL = 'http://demo1781626.mockable.io/';

export const errorMessages = {
    firstName: {
        required: 'First Name is mandatory'
    },
    lastName: {
        required: 'Last Name is mandatory'
    },
    email: {
        required: 'Email is mandatory',
        supportedDomain: 'Should have an ces emailid',
        invalid: 'Email id is not valid'
    },
    phone: {
        required: 'Mobile number is mandatory',
        minLength: 'Mobile number should be 10 digits',
        pattern: 'Enter valid mobile number'
    },
    password: {
        required: 'Password is mandatory',
        minLength: 'Password sholuld be 6 characters long',
        pattern: 'Should include minimum 1 digit, 1 special character'
    },
    confirmPassword: {
        required: 'Confirm Password is mandatory',
    }
};

export let order = {
    products: {} as model.Product,
    totalPrice: 0,
    billingAddress: '',
    status: 'New',
    orderedDate: new Date()
};


export const AppInitialState = {
    store: {
        customer: {} as model.Customer,
        loggedIn: false,
        cartInfo: [],
        orders: []
    }
};
