import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgModel } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material';

import { SignupComponent } from '../signup/signup.component';
import { LoginService } from './login.service';
import { errorMessages } from '../../app/constants';

import { AppState } from '../../app/Model/app.state.model';
import * as appActions from '../../app/app.actions';
import * as model from '../../app/Model/customer.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide: boolean;
  customer: model.Customer = {} as model.Customer;
  private errorMessages: any;
  private dialogRef;
  constructor(private router: Router, public dialog: MatDialog, private loginService: LoginService, private store: Store<AppState>) {
  }
  ngOnInit() {
    this.hide = true;
    this.errorMessages = errorMessages;
  }
  // Login Validation
  login() {
    this.loginService.validateCustomer$(this.customer).subscribe((data) => {
      if (data.length === 0) {
        alert('invalid user');
      } else {
        this.store.dispatch(new appActions.LoadCustomerSuccessAction(data[0]));
        this.store.dispatch(new appActions.LoggedInSuccessAction(true));
        sessionStorage.setItem('cusid', data[0].customerId);
        this.router.navigate(['home/dashboard']);
      }
    });
  }
  // Opening the SignUp in a dialog component
  openRegisterForm() {
    this.dialogRef = this.dialog.open(SignupComponent, { disableClose: true });
    this.dialogRef.componentInstance.registeredSuccessful.subscribe(() => {
      this.dialog.closeAll();
      this.router.navigateByUrl('login');
    });
  }
  // Unsubscribe the dialog
  ngonDestroy() {
    this.dialogRef.componentInstance.registeredSuccessful.unsubscribe();
  }
}
