import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Customer } from '../../app/Model/customer.model';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) {
  }
  validateCustomer$(user: Customer): Observable<any> {
    return this.http.get('../assets/backend/customersDatabase.json').pipe(
      map((response: any) => {
        const data = response.customers;
        const token = response.token;
        const customer = data.filter((item: Customer) => (item.phone.trim() === user.phone.trim() && item.password === user.password));
        localStorage.setItem('tokenId', token);
        return customer;
      })
    );
  }
}
