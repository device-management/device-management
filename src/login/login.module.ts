import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { SignupComponent } from './signup/signup.component';
import { EmailCheckerDirective } from './signup/emailChecker.directive';
const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' }
];

@NgModule({
  declarations: [LoginComponent, SignupComponent, EmailCheckerDirective],
  imports: [
    CommonModule,
    FormsModule,
    CustomMaterialModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [
    SignupComponent
  ],
})
export class LoginModule { }
