import { Directive } from '@angular/core';
import { Validator, NG_VALIDATORS } from '@angular/forms';
import { AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appEmailChecker]',
  providers: [{ provide: NG_VALIDATORS, useExisting: EmailCheckerDirective, multi: true }]
})
export class EmailCheckerDirective implements Validator {
  allowedMailExtensions = ['cesltd.com', 'gmail.com'];
  constructor() { }
  validate(control: AbstractControl) {
    if (control.value) {
      const domain = control.value.split('@')[1];
      if (!this.allowedMailExtensions.includes(domain)) {
        return { supportedDomain: 'Should have an ces/gmail account' };
      }
      return null;
    }
  }
}
