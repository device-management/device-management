import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgModel, NgForm } from '@angular/forms';
import { Customer } from 'src/app/Model/customer.model';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/Model/app.state.model';
import * as appActions from '../../app/app.actions';
import { errorMessages } from '../../app/constants';
import { MatDialog } from '@angular/material';
import { EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {
  hide: boolean;
  customer: Customer = {} as Customer;
  showConfirmPassword: boolean;
  errorMessages: any;
  action: string;
  loggedIn: boolean;
  @Output() registeredSuccessful: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, private store: Store<AppState>) { }
  ngOnInit() {
    this.hide = true;
    this.loggedIn = false;
    this.action = 'Register';
    this.errorMessages = errorMessages;
    // Finds new user or edit the profile
    this.store.select((state) => state.store).subscribe((data) => {
      if (data.loggedIn) {
        this.loggedIn = true;
        this.action = 'Update';
        this.customer = data.customer;
      } else {
        this.action = 'Register';
      }
    });
  }
  register() {
    alert('Registered successfully');
    this.registeredSuccessful.emit();
  }
  update() {
    this.store.dispatch(new appActions.UpdateCustomerDetailsAction(this.customer));
    alert('Updated successfully');
    this.router.navigateByUrl('home/dashboard');
  }
  cancel() {
  if (!this.loggedIn) {
    this.registeredSuccessful.emit();
  } else {
    this.router.navigateByUrl('home/dashboard');
  }
  }
 }
